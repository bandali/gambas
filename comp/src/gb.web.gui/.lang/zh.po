#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gb.web.gui 3.15.90\n"
"POT-Creation-Date: 2023-03-02 01:09 UTC\n"
"PO-Revision-Date: 2020-11-18 18:43 UTC\n"
"Last-Translator: Benoît Minisini <g4mba5@gmail.com>\n"
"Language: zh\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "Web application development using processes as session"
msgstr "使用进程作为会话的Web应用程序开发"

#: FInputBox.webform:41 FMessage.webform:45 Webform1.webform:169
msgid "OK"
msgstr "确定"

#: FInputBox.webform:49 Webform2.webform:39
msgid "Cancel"
msgstr ""

#: FTestDialog.webform:19 Webform1.webform:163
msgid "Tab 1"
msgstr ""

#: FTestDialog.webform:24 Webform1.webform:263
msgid "Tab 2"
msgstr ""

#: FTestDialog.webform:29 Webform1.webform:297
msgid "Tab 3"
msgstr ""

#: FTestDrawingArea.webform:23
msgid "Refresh"
msgstr ""

#: FTestWebAudio.webform:24
msgid "Play"
msgstr ""

#: FTestWebAudio.webform:30
msgid "Pause"
msgstr ""

#: FTestWebAudio.webform:36
msgid "Stop"
msgstr ""

#: FTestWebAudio.webform:49
msgid "Air Life"
msgstr ""

#: FTestWebAudio.webform:49
msgid "Balanced"
msgstr ""

#: FTestWebAudio.webform:49
msgid "Just live"
msgstr ""

#: Message.class:34
msgid "Information"
msgstr "信息"

#: Message.class:46
msgid "Warning"
msgstr "警告"

#: Message.class:52
msgid "Error"
msgstr "错误"

#: Message.class:58
msgid "Question"
msgstr "问题"

#: WebAudio.class:74
msgid "Html audio not supported"
msgstr ""

#: Webform1.webform:79
msgid "WebMenu1"
msgstr ""

#: Webform1.webform:82
msgid "Open"
msgstr ""

#: Webform1.webform:88
msgid "Open recent"
msgstr ""

#: Webform1.webform:92
msgid "WebMenu12"
msgstr ""

#: Webform1.webform:96
msgid "WebMenu13"
msgstr ""

#: Webform1.webform:99
msgid "WebMenu15"
msgstr ""

#: Webform1.webform:104
msgid "WebMenu16"
msgstr ""

#: Webform1.webform:109
msgid "WebMenu17"
msgstr ""

#: Webform1.webform:116
msgid "WebMenu14"
msgstr ""

#: Webform1.webform:124
msgid "Save"
msgstr ""

#: Webform1.webform:130
msgid "Save as"
msgstr ""

#: Webform1.webform:138
msgid "Close"
msgstr ""

#: Webform1.webform:145
msgid "WebMenu2"
msgstr ""

#: Webform1.webform:148
msgid "WebMenu5"
msgstr ""

#: Webform1.webform:152
msgid "WebMenu6"
msgstr ""

#: Webform1.webform:189
msgid "Remove tab 2"
msgstr ""

#: Webform1.webform:193
msgid "Logout"
msgstr ""

#: Webform1.webform:197
msgid "Select first"
msgstr ""

#: Webform1.webform:201
msgid "Bip"
msgstr ""

#: Webform1.webform:205
msgid "Check"
msgstr ""

#: Webform1.webform:216 Webform3.webform:18
msgid "Élément 1"
msgstr ""

#: Webform1.webform:216 Webform3.webform:18
msgid "Élément 2"
msgstr ""

#: Webform1.webform:216 Webform3.webform:18
msgid "Élément 3"
msgstr ""

#: Webform1.webform:224
msgid "Reload"
msgstr ""

#: Webform1.webform:229
msgid "Select file"
msgstr ""

#: Webform1.webform:243
msgid "Test label alignment"
msgstr ""

#: Webform1.webform:259
msgid "Call me"
msgstr ""

#: Webform1.webform:268
msgid "Alpha"
msgstr ""

#: Webform1.webform:272
msgid "Beta"
msgstr ""

#: Webform1.webform:276
msgid "Gamma"
msgstr ""

#: Webform1.webform:282
msgid "<i>Élément 5</i>"
msgstr ""

#: Webform1.webform:282
msgid "<span style=\"color:red;\">Élément 9</span>"
msgstr ""

#: Webform1.webform:282 Webform3.webform:28
msgid "Élément 10"
msgstr ""

#: Webform1.webform:282 Webform3.webform:28
msgid "Élément 11"
msgstr ""

#: Webform1.webform:282 Webform3.webform:28
msgid "Élément 12"
msgstr ""

#: Webform1.webform:282 Webform3.webform:18
msgid "Élément 4"
msgstr ""

#: Webform1.webform:282 Webform3.webform:18
msgid "Élément 6"
msgstr ""

#: Webform1.webform:282 Webform3.webform:28
msgid "Élément 7"
msgstr ""

#: Webform1.webform:282 Webform3.webform:28
msgid "Élément 8"
msgstr ""

#: Webform1.webform:289
msgid "Copy"
msgstr ""

#: Webform1.webform:303
msgid "Check me"
msgstr ""

#: Webform1.webform:311
msgid "Show me"
msgstr ""

#: Webform1.webform:315
msgid "Progress"
msgstr ""

#: Webform1.webform:319
msgid "Highlight"
msgstr ""

#: Webform1.webform:323
msgid "InputBox"
msgstr ""

#: Webform1.webform:327
msgid "Test dialog"
msgstr ""

#: Webform1.webform:356
msgid "Tab 4"
msgstr ""

#: Webform1.webform:366
msgid "Multiple"
msgstr ""

#: Webform2.webform:21
msgid "Traitement en cours"
msgstr ""

#: Webform3.webform:18
msgid "Élément 5"
msgstr ""

#: Webform3.webform:28
msgid "Élément 9"
msgstr ""

#: Webform4.webform:35
msgid "bonjour"
msgstr ""

#: Webform4.webform:48
msgid "test"
msgstr ""

#: Webform4.webform:61
msgid "Agence"
msgstr ""
